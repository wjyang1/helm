kubectl get ns $NAMESPACE > /dev/null 2>&1 || kubectl create ns $NAMESPACE
kubectl get configmap $CONFIGMAP_NAME -n $NAMESPACE >/dev/null 2>&1 && echo "ConfigMap already exists" || kubectl create configmap $CONFIGMAP_NAME -n $NAMESPACE --from-env-file=$DATE_GATEWAY_INFRA_ENV_FILE --from-env-file=$DATE_GATEWAY_ENV_FILE
helm upgrade ${app_name} --install --create-namespace -n $NAMESPACE \
                        --set image.repository=${common_envs.REGISTRY_PATH}/$TARGET_LABEL/${item}/${item} \
                        --set image.tag=$IMAGE_TAG \
                        --set configMap=$CONFIGMAP_NAME \
                        --set fullnameOverride=${app_name} \
                        --set service.type=$SERVICE_TYPE \
                        --set service.port=$SERVICE_PORT \
                        --set service.containerPort=$CONTAINER_PORT \
                        ${env.WORKSPACE}/chatbot/persona/.

